package org.shibuka.planetinhas;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.shibuka.planetinhas.dto.interno.PlanetaAPIStarWarsResponse;
import org.shibuka.planetinhas.service.interno.StarWarsService;

@Path("/api/star-wars")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "star-wars-api")
public class StarWarsResource {

    @Inject
    StarWarsService starWarsService;

    @GET
    @Path("/planetas")
    public List<PlanetaAPIStarWarsResponse> buscaPlanetasAPIStarWars(@DefaultValue("1") @QueryParam("page") int page) {
        return starWarsService.findPlanetas(page);
    }    
}