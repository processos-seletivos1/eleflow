package org.shibuka.planetinhas;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.shibuka.planetinhas.dto.interno.AdicionarPlanetaDTO;
import org.shibuka.planetinhas.dto.interno.PlanetaDTO;
import org.shibuka.planetinhas.service.interno.PlanetaService;
import org.shibuka.planetinhas.service.interno.StarWarsService.NumeroInvalidoPlanetaApiExterno;

@Path("/api/planetas")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Tag(name = "planetas")
public class PlanetaResource {

    @Inject
    PlanetaService service;

    private Optional<Integer> optionalOfPage(int page) {
        return page > 0 ? Optional.of(page) : Optional.empty();
    }

    @GET
    public List<PlanetaDTO> buscarPlanetas(@DefaultValue("0") @QueryParam("page") int page) {
        return service.buscarTodosPlanetas(optionalOfPage(page));
    }

    @GET
    @Path("/{id}")
    public PlanetaDTO findById(@PathParam("id") Long id) {
        return service.findById(id);
    }

    @GET
    @Path("/nome/{nome}")
    public List<PlanetaDTO> findById(@PathParam("nome") String nome, @DefaultValue("0") @QueryParam("page") int page) {
        return service.findByNome(nome, optionalOfPage(page));
    }

    @POST
    @Transactional
    public Response adicionar(AdicionarPlanetaDTO dto) {        
        try {
            URI location = service.adicionar(dto);

            return Response.created(location).build();
        } catch (NumeroInvalidoPlanetaApiExterno e) {           
            return Response.serverError().build();
        }

    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response removerItem(
            @PathParam("id") Long id
    ) {
        service.remover(id);

        return Response.noContent().build();
    }
}