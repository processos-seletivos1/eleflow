package org.shibuka.planetinhas.service.interno;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.shibuka.planetinhas.dto.externo.PlanetaApiStarWarsDto;
import org.shibuka.planetinhas.dto.externo.PlanetasApiStarWarsDto;
import org.shibuka.planetinhas.dto.interno.PlanetaAPIStarWarsResponse;
import org.shibuka.planetinhas.exceptions.BaseException;
import org.shibuka.planetinhas.mapper.PlanetaAPiStarWarsMapper;
import org.shibuka.planetinhas.service.externo.StarWarsApiExternaService;


@ApplicationScoped
public class StarWarsService {

    public static class NumeroInvalidoPlanetaApiExterno extends BaseException
    {
        private static final long serialVersionUID = 3555714415375055302L;
        public NumeroInvalidoPlanetaApiExterno(String nome) {
            super("Número de planetas retornado pela API externa é maior do que o esperado para o nome: " + nome);
        }
    }

    @Inject
    @RestClient
    StarWarsApiExternaService apiStarWarsService;

    @Inject
    PlanetaAPiStarWarsMapper mapper;

    public List<PlanetaAPIStarWarsResponse> findPlanetas(Integer page) {
        try {
            Stream<PlanetaApiStarWarsDto> s = apiStarWarsService.findPlanetas(page).planetas.stream();

            return s.map(mapper::toResponseDTO).collect(Collectors.toList());
        } catch (WebApplicationException e) {
            if (Status.NOT_FOUND.getStatusCode() == e.getResponse().getStatus()) {
                return new ArrayList<PlanetaAPIStarWarsResponse>();
            }

            throw new RuntimeException();
        }
    }

    public Integer buscarQuantidadeAparicoesFilmesDoPlaneta(String nome) throws NumeroInvalidoPlanetaApiExterno {
        PlanetasApiStarWarsDto dto = apiStarWarsService.searchPlaneta(nome);

         if (dto.planetas.isEmpty()) {
             return 0;
         }

         if (dto.planetas.size() >= 2) {
             throw new NumeroInvalidoPlanetaApiExterno(nome);
         };

        return dto.planetas.get(0).filmes.size();
    }
}
