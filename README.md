
# Desafio técnico backend - Eleflow

## Dependencies
- Java 11+
- Docker

## Instructions

```
❯ docker run --rm -d -p 3306:3306 \
    --name=db \
    --network=eleflow-default \
    -e MYSQL_ROOT_PASSWORD=planetinhas \
    -e MYSQL_DATABASE=planetinhas \
    -e MYSQL_USER=planetinhas \
    -e MYSQL_PASSWORD=planetinhas \
    mysql:8.0
❯ cd backend
❯ ./mvnw compile quarkus:dev
```

## Backend

API documentation:
- http://localhost:8080/q/swagger-ui/#/

**Framework**:
- Quarkus

**Extensions:**
- quarkus-resteasy
- quarkus-resteasy-jsonb
- quarkus-hibernate-orm-panache
- quarkus-hibernate-validator
- quarkus-smallrye-openapi
- quarkus-myysql-postgresql
- quarkus-flyway

**Others dependencies**
- Database Rider
- Test Containers
- Map Struct

# Pipeline

Foi configurado um pipeline que realiza um deploy para a amazon após a criação de uma TAG.
