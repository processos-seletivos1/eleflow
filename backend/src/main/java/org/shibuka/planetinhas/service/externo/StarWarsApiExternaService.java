package org.shibuka.planetinhas.service.externo;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.shibuka.planetinhas.dto.externo.PlanetasApiStarWarsDto;

@RegisterRestClient(configKey = "starwars-api")
@Produces(MediaType.APPLICATION_JSON)
public interface StarWarsApiExternaService {

    @GET
    @Path("/planets/")
    PlanetasApiStarWarsDto findPlanetas(@QueryParam("page") int page);


    @GET
    @Path("/planets/")
    PlanetasApiStarWarsDto searchPlaneta(@QueryParam("search") String nome);    

}
