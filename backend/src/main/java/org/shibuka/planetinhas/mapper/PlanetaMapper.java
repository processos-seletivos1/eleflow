package org.shibuka.planetinhas.mapper;

import org.mapstruct.Mapper;
import org.shibuka.planetinhas.domain.Planeta;
import org.shibuka.planetinhas.dto.interno.AdicionarPlanetaDTO;
import org.shibuka.planetinhas.dto.interno.PlanetaDTO;

@Mapper(componentModel = "cdi")
public interface PlanetaMapper {    

    public Planeta toPlaneta(AdicionarPlanetaDTO dto);

    public PlanetaDTO toDto(Planeta planeta);
}