package org.shibuka.planetinhas.configuration;

import java.util.HashMap;
import java.util.Map;

import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.utility.DockerImageName;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

public class BackendTestLifecycle implements QuarkusTestResourceLifecycleManager {
            
    private static MySQLContainer<?> MYSQL = new MySQLContainer<>(
        DockerImageName.parse("mysql/mysql-server:8.0").asCompatibleSubstituteFor("mysql")
    );
    
    @Override
    public Map<String, String> start() {
        MYSQL.start();

        final Map<String, String> propriedades = new HashMap<>();
        
		propriedades.put("quarkus.datasource.jdbc.url",MYSQL.getJdbcUrl());
        propriedades.put("quarkus.datasource.username",MYSQL.getUsername());
        propriedades.put("quarkus.datasource.password",MYSQL.getPassword());

        return propriedades;
    }

    @Override
    public void stop() {
        if (MYSQL != null) {
            MYSQL.stop();
        }
    }
}
