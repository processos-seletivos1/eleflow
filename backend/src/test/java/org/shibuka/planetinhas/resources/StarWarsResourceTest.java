package org.shibuka.planetinhas.resources;

import static io.restassured.RestAssured.given;

import javax.ws.rs.core.Response.Status;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;
import org.shibuka.planetinhas.configuration.WireMockAPIStarWars;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(WireMockAPIStarWars.class)
public class StarWarsResourceTest {
	private static final String ENDPOINT = "/api/star-wars";

	@Test
    public void seRealizadoBuscaDePlanetasSemParametroPageEntaoPaginacaoNaoDeveSerRespeitada() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.when().get(ENDPOINT + "/planetas")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    public void seRealizadoBuscaDePlanetasParametroPageComValor1EntaoPaginacaoDeveSerRespeitada() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.param("page", 2)
					.when().get(ENDPOINT + "/planetas")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    public void seRealizadoBuscaDePlanetasParametroPageComValor2EntaoPaginacaoDeveSerRespeitada() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.param("page", 2)
					.when().get(ENDPOINT + "/planetas")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    public void seRealizadoBuscaDePlanetasParametroPageComValor7EntaoDeveRetornaVazio() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.param("page", 7)
					.when().get(ENDPOINT + "/planetas")					
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}		
}