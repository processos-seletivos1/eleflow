package org.shibuka.planetinhas.dto.interno;

public class PlanetaDTO {

    public Long id;
    public String nome;
    public String clima;
    public String terreno;
    public int quantidadeAparicoesFilmes;
    
}
