package org.shibuka.planetinhas.service.interno;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.shibuka.planetinhas.domain.Planeta;
import org.shibuka.planetinhas.dto.interno.AdicionarPlanetaDTO;
import org.shibuka.planetinhas.dto.interno.PlanetaDTO;
import org.shibuka.planetinhas.mapper.PlanetaMapper;
import org.shibuka.planetinhas.service.interno.StarWarsService.NumeroInvalidoPlanetaApiExterno;

import io.quarkus.hibernate.orm.panache.PanacheQuery;

@ApplicationScoped
public class PlanetaService {

    @Inject
    StarWarsService starWarsService;

    @Inject
    PlanetaMapper mapper;

    @ConfigProperty(name = "api.pagination.pageSize") 
    private int pageSize;
        
    public URI adicionar(AdicionarPlanetaDTO dto) throws NumeroInvalidoPlanetaApiExterno {
        Planeta planeta = mapper.toPlaneta(dto);

        planeta.quantidadeAparicoesFilmes = starWarsService.buscarQuantidadeAparicoesFilmesDoPlaneta(planeta.nome);

        planeta.persistAndFlush();

        return URI.create("/" + planeta.id);
    }


    public List<PlanetaDTO> buscarTodosPlanetas(Optional<Integer> page) {
        Stream<Planeta> s = paginar(Planeta.findAll(), page);

		return s.map(mapper::toDto).collect(Collectors.toList());
	}

	public void remover(Long id) {
        Planeta planeta = buscarPlaneta(id);

        planeta.delete();
	}

    public List<PlanetaDTO> findByNome(String nome, Optional<Integer> page) {
        Stream<Planeta> s = paginar(Planeta.find("nome", nome), page);

		return s.map(mapper::toDto).collect(Collectors.toList());
    }

    public PlanetaDTO findById(Long id) {
        Planeta planeta = buscarPlaneta(id);

        return mapper.toDto(planeta);
    }

    private Stream<Planeta> paginar(PanacheQuery<Planeta> query, Optional<Integer> page){
        if (page.isPresent())
        return query.page(page.get()-1, pageSize).stream();

        return query.stream();
    }

    private Planeta buscarPlaneta(Long id) {
        final Optional<Planeta> op = Planeta.findByIdOptional(id);

        if (op.isEmpty()) {
            throw new NotFoundException();
        }

        return op.get();
    }

}
