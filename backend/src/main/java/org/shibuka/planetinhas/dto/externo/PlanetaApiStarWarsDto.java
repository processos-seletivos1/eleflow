package org.shibuka.planetinhas.dto.externo;

import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

public class PlanetaApiStarWarsDto {
    
    @JsonbProperty("name")
    public String nome;

    @JsonbProperty("climate")
    public String clima;
    
    @JsonbProperty("terrain")
    public String terreno;

    @JsonbProperty("films")
    public List<String> filmes;    
}
