package org.shibuka.planetinhas.configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;

import javax.ws.rs.core.Response.Status;

import com.github.tomakehurst.wiremock.WireMockServer;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WireMockAPIStarWars implements QuarkusTestResourceLifecycleManager {
	private WireMockServer wireMockServer;

	@Override
	public Map<String, String> start() {
		wireMockServer = new WireMockServer();
		wireMockServer.start();

		Path rdApiPlanetas1= Paths.get("src", "test", "resources", "wiremock", "star-wars-api-planets-1.json");
		Path rdApiPlanetas2 = Paths.get("src", "test", "resources", "wiremock", "star-wars-api-planets-2.json");
		Path rdApiPlanetas7 = Paths.get("src", "test", "resources", "wiremock", "star-wars-api-planets-7.json");
		Path rdApiSearchPlanetaAlderaan = Paths.get("src", "test", "resources", "wiremock", "star-wars-api-search-planet-Alderaan.json");		

		try {
			stubFor(get(urlEqualTo("/planets/"))
				.willReturn(
					aResponse()
					.withHeader("Content-Type", "application/json")
					.withBody(Files.readString(rdApiPlanetas1))));
			stubFor(get(urlEqualTo("/planets/?page=1"))
				.willReturn(
					aResponse()
					.withHeader("Content-Type", "application/json")
					.withBody(Files.readString(rdApiPlanetas1))));
			stubFor(get(urlEqualTo("/planets/?page=2"))
				.willReturn(
					aResponse()
					.withHeader("Content-Type", "application/json")
					.withBody(Files.readString(rdApiPlanetas2))));
			stubFor(get(urlEqualTo("/planets/?page=7"))
				.willReturn(
					aResponse()
					.withHeader("Content-Type", "application/json")
					.withStatus(Status.NOT_FOUND.getStatusCode())
					.withBody(Files.readString(rdApiPlanetas7))));
			stubFor(get(urlEqualTo("/planets/?search=Alderaan"))
				.willReturn(
					aResponse()
					.withHeader("Content-Type", "application/json")
					.withBody(Files.readString(rdApiSearchPlanetaAlderaan))));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		stubFor(get(urlMatching(".*")).atPriority(10).willReturn(aResponse().proxiedFrom("http://localhost:8081")));

		return Collections.singletonMap("org.shibuka.planetinhas.service.StarWarsApiExternaService/mp-rest/url",
				wireMockServer.baseUrl());
	}

	@Override
	public void stop() {
		if (null != wireMockServer) {
			wireMockServer.stop();
		}
	}
}
