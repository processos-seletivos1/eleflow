variable "aws_acess_key" {
  type = string
}

variable "aws_secret_key" {
  type = string
}

variable "aws_ami" {
  type = string
}

variable "aws_type" {
  type = string
}

variable "aws_key_name" {
  type = string
}
variable "aws_region" {
  type = string
}

variable "aws_availability_zone" {
  type = string
}

terraform {
  backend "s3" {
    # Lembre de trocar o bucket para o seu, não pode ser o mesmo nome
    bucket         = "descomplicando-terraform-shibuka-tfstates"
    dynamodb_table = "terraform-state-lock-dynamo"
    key            = "terraform-test.tfstate"
    region         = "us-east-1"
    encrypt        = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region  = "us-east-1"
  version = "~> 3.0"
}


# 1 - Create VPC
resource "aws_vpc" "prod-vpc" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "prod"
  }
}

# 2 - Create Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.prod-vpc.id
}

# 3 - Create Custom Route Table
resource "aws_route_table" "prod-route-table" {
  vpc_id = aws_vpc.prod-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id      = aws_internet_gateway.gw.id
  }

  tags = {
    Name = "prod"
  }
}

# 4 - Create a Subnet
resource "aws_subnet" "subnet-1" {
  vpc_id            = aws_vpc.prod-vpc.id
  cidr_block        = "10.0.1.0/24"
  availability_zone = var.aws_availability_zone

  tags = {
    Name = "prod-subnet"
  }
}

# 5 - Associate subnet with Route Table

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.subnet-1.id
  route_table_id = aws_route_table.prod-route-table.id
}


# 6 - Create Security Group to allow port 22, 80, 443, 8080
resource "aws_security_group" "allow_web" {
  name        = "allow_web_traffic"
  description = "Allow Web inbound traffic"
  vpc_id      = aws_vpc.prod-vpc.id

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "BACKEND"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_web"
  }
}

# 7 - Create a network interface with an ip in the subnet that was created in step 4
resource "aws_network_interface" "web-server-nic" {
  subnet_id       = aws_subnet.subnet-1.id
  private_ips     = ["10.0.1.50"]
  security_groups = [aws_security_group.allow_web.id]
}

# 8 - Assign an elastic IP to the network interface created in the step 7
resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.web-server-nic.id
  associate_with_private_ip = "10.0.1.50"
  # Should be created after intert gateway or it throw an error
  depends_on = [aws_internet_gateway.gw]
}

# Shows the public ip after a terraform apply
output "server_public_ip" {
  value = aws_eip.one.public_ip
}

# 9 - Create Ubuntu server and install/enable apache2
resource "aws_instance" "web-server-instance" {
  ami               = var.aws_ami
  instance_type     = var.aws_type
  availability_zone = var.aws_availability_zone
  key_name          = var.aws_key_name

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.web-server-nic.id
  }

  user_data = <<-EOF
                #! /bin/bash
                sudo apt update -y
                sudo curl -fsSL https://get.docker.com | bash
                docker network create -d bridge eleflow-default
                docker run --rm -d -p 3306:3306 \
                  --name=db \
                  --network=eleflow-default \
                  -e MYSQL_ROOT_PASSWORD=planetinhas \
                  -e MYSQL_DATABASE=planetinhas \
                  -e MYSQL_USER=planetinhas \
                  -e MYSQL_PASSWORD=planetinhas \
                  mysql:8.0
                sleep 30
                docker run -i --rm -d -p 8080:8080 \
                  --network=eleflow-default \
                  --name=backend \
                  eduardoshibukawa/backend-planetinhas:latest
                EOF

  tags = {
    Name = "web-server"
  }
}

# Prints the private ip of web server instance
output "server_private_ip" {
  value = aws_instance.web-server-instance.private_ip
}

# Prints the id of web server instance
output "server_id" {
  value = aws_instance.web-server-instance.id
}
