package org.shibuka.planetinhas.resources;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Optional;

import javax.ws.rs.core.Response.Status;

import com.github.database.rider.cdi.api.DBRider;
import com.github.database.rider.core.api.configuration.DBUnit;
import com.github.database.rider.core.api.configuration.Orthography;
import com.github.database.rider.core.api.dataset.DataSet;

import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;
import org.shibuka.planetinhas.configuration.BackendTestLifecycle;
import org.shibuka.planetinhas.configuration.WireMockAPIStarWars;
import org.shibuka.planetinhas.domain.Planeta;
import org.shibuka.planetinhas.dto.interno.AdicionarPlanetaDTO;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import io.restassured.http.Headers;
import io.restassured.specification.RequestSpecification;

@DBRider
@DBUnit(caseInsensitiveStrategy = Orthography.LOWERCASE)
@QuarkusTest
@QuarkusTestResource(BackendTestLifecycle.class)
@QuarkusTestResource(WireMockAPIStarWars	.class)
public class PlanetaResourceTest {
	private static final String ENDPOINT = "/api/planetas";
    private static final String CENARIO_1_YML = "cenario-planeta-yavin-iv.yml";
	private static final String CENARIO_2_YML = "cenario-com-dois-planetas.yml";
	private static final String CENARIO_3_YML = "cenario-com-seis-planetas.yml";

	@Test
    @DataSet(CENARIO_1_YML)
    public void testGetEndpointCenario1() {
		final String resultado =
				given()
					.contentType(ContentType.JSON)
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    @DataSet(CENARIO_2_YML)
    public void testGetEndpointCenario2() {
		final String resultado =
				givenJson()
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

    @Test
	@DataSet(CENARIO_3_YML)
	public void seRealizadoBuscaDeTodosSemParametroPageEntaoPaginacaoNaoDeveSerRespeitada() {
		final String resultado =
				givenJson()
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

    @Test
	@DataSet(CENARIO_3_YML)
	public void seRealizadoBuscaDeTodosComParametroPageEntaoPaginacaoDeveSerRespeitada() {
		final String resultado =
				givenJson()
					.param("page", 1)
					.when().get(ENDPOINT)
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    @DataSet(CENARIO_2_YML)
    public void testGetNomeEndpoint() {
		final String resultado =
				givenJson()
					.with().pathParam("nome", "Tatooine")
					.when().get(ENDPOINT + "/nome/{nome}")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    @DataSet(CENARIO_2_YML)
    public void seRealizadoBuscaDeNomeInexistenteEntaoRetornoDeveSerVazio() {
		final String resultado =
				givenJson()
					.with().pathParam("nome", "Vegeta")
					.when().get(ENDPOINT + "/nome/{nome}")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    @DataSet(CENARIO_2_YML)
    public void testGetIdEndpoint() {
		final String resultado =
				givenJson()
					.with().pathParam("id", 1)
					.when().get(ENDPOINT + "/{id}")
					.then()
						.statusCode(Status.OK.getStatusCode())
						.extract().asString();

		Approvals.verifyAsJson(resultado);
	}

	@Test
    @DataSet(CENARIO_2_YML)
    public void seRealizadoBuscaDeIdInexistenteEntaoRetornoDeveSer404() {
		givenJson()
			.with().pathParam("id", 3)
			.when().get(ENDPOINT + "/{id}")
			.then()
				.statusCode(Status.NOT_FOUND.getStatusCode());
	}


    @Test
	@DataSet(CENARIO_1_YML)	
	public void SeRealizadoUmPostEntaoNovaQuantidadeDePlanetasDeveSer2() {
		final AdicionarPlanetaDTO dto = new AdicionarPlanetaDTO();

		dto.nome = "Alderaan";
		dto.clima = "temperate";
		dto.terreno = "grasslands, mountains";

		givenJson()
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode())
			;

			final Long quantidadeEsperada = 2L;
			final Long quantidadeAtual = Planeta.count();

		assertEquals(quantidadeEsperada, quantidadeAtual, "Quantidade de planetas deve ser 2 após post");
	}

    @Test
    @DataSet(CENARIO_2_YML)
	public void SeRealizadoUmPostEntaoNovaQuantidadeDePlanetasDeveSer3() {
		final AdicionarPlanetaDTO dto = new AdicionarPlanetaDTO();

		dto.nome = "Alderaan";
		dto.clima = "temperate";
		dto.terreno = "grasslands, mountains";

		givenJson()
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode())
			;

			final Long quantidadeEsperada = 3L;
			final Long quantidadeAtual = Planeta.count();

		assertEquals(quantidadeEsperada, quantidadeAtual, "Quantidade de planetas deve ser 3 após post");
	}

    @Test
	@DataSet(CENARIO_1_YML)
	public void seRealizadoPostDePlanetaEntaoDeveExistirHeaderLocation() {
		final AdicionarPlanetaDTO dto = new AdicionarPlanetaDTO();

		dto.nome = "Alderaan";
		dto.clima = "temperate";
		dto.terreno = "grasslands, mountains";

		Headers headers = givenJson()
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode())
			.extract()
			.headers();

		assertTrue("Header deve existir", headers.exist());
		assertNotNull("Deve existir header location", headers.get("location"));
	}

    @Test
	@DataSet(CENARIO_1_YML)
	public void seRealizadoPostDePlanetaAlderaanEntaoQuantidadeDeFilmesDeveSer2() {
		final AdicionarPlanetaDTO dto = new AdicionarPlanetaDTO();

		dto.nome = "Alderaan";
		dto.clima = "temperate";
		dto.terreno = "grasslands, mountains";

		String id = givenJson()
            .body(dto)
            .when().post(ENDPOINT)
            .then()
            .statusCode(Status.CREATED.getStatusCode())
			.extract()
			.header("location")
			.substring(22);		
						
		Optional<Planeta> op = Planeta.findByIdOptional(Long.valueOf(id));

		final Integer quantidadeEsperada = 2;
		final Integer quantidadeAtual = op.isPresent() ? op.get().quantidadeAparicoesFilmes : 0;

		assertEquals(quantidadeEsperada, quantidadeAtual, "Quantidade de filmes do planeta Alderaan deve ser 2 após post");
	}

	@Test
	@DataSet(CENARIO_2_YML)
	public void seRealizadoUmDeleteNoEndpointEntaoNovaQuantidadeDePlanetasDeveSer1() {
		givenJson()
			.with().pathParam("id", 1L)
			.when().delete(ENDPOINT + "/{id}")
			.then()
				.statusCode(Status.NO_CONTENT.getStatusCode());

		final Long quantidadeEsperada = 1L;
		final Long quantidadeAtual = Planeta.count();

		assertEquals(quantidadeEsperada, quantidadeAtual, "Quantidade de planetas deve ser 1 após delete");
	}

	@Test
	@DataSet(CENARIO_1_YML)
	public void seRealizadoUmDeleteNoEndpointEntaoNovaQuantidadeDePlanetasDeveSer0() {
		givenJson()
			.with().pathParam("id", 1L)
			.when().delete(ENDPOINT + "/{id}")
			.then()
				.statusCode(Status.NO_CONTENT.getStatusCode());

		final Long quantidadeEsperada = 0L;
		final Long quantidadeAtual = Planeta.count();

		assertEquals(quantidadeEsperada, quantidadeAtual, "Quantidade de planetas deve ser 0 após delete");
	}

	private RequestSpecification givenJson() {
		return given()
			.contentType(ContentType.JSON);
	}
}