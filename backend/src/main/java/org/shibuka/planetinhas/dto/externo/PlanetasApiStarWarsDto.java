package org.shibuka.planetinhas.dto.externo;

import java.util.List;

import javax.json.bind.annotation.JsonbProperty;

public class PlanetasApiStarWarsDto {
    
    @JsonbProperty("results")
    public List<PlanetaApiStarWarsDto> planetas;
    
}
