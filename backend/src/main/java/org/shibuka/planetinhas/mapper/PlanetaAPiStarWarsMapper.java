package org.shibuka.planetinhas.mapper;

import org.mapstruct.Mapper;
import org.shibuka.planetinhas.dto.externo.PlanetaApiStarWarsDto;
import org.shibuka.planetinhas.dto.interno.PlanetaAPIStarWarsResponse;

@Mapper(componentModel = "cdi")
public interface PlanetaAPiStarWarsMapper {    

    public PlanetaAPIStarWarsResponse toResponseDTO(PlanetaApiStarWarsDto dto);

}