CREATE TABLE planeta (
	id BIGINT NOT NULL AUTO_INCREMENT,	
	nome varchar(255) NOT NULL,
	clima varchar(255) NOT NULL,
	terreno varchar(255) NOT NULL,
	quantidadeaparicoesfilmes TINYINT, 

    PRIMARY KEY (id)
);